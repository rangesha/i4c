require 'test_helper'

class ApiVendorsControllerTest < ActionController::TestCase
  setup do
    @api_vendor = api_vendors(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:api_vendors)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create api_vendor" do
    assert_difference('ApiVendor.count') do
      post :create, api_vendor: { client_id: @api_vendor.client_id, client_secret: @api_vendor.client_secret }
    end

    assert_redirected_to api_vendor_path(assigns(:api_vendor))
  end

  test "should show api_vendor" do
    get :show, id: @api_vendor
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @api_vendor
    assert_response :success
  end

  test "should update api_vendor" do
    patch :update, id: @api_vendor, api_vendor: { client_id: @api_vendor.client_id, client_secret: @api_vendor.client_secret }
    assert_redirected_to api_vendor_path(assigns(:api_vendor))
  end

  test "should destroy api_vendor" do
    assert_difference('ApiVendor.count', -1) do
      delete :destroy, id: @api_vendor
    end

    assert_redirected_to api_vendors_path
  end
end
