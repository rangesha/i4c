json.array!(@api_vendors) do |api_vendor|
  json.extract! api_vendor, :id, :client_id, :client_secret
  json.url api_vendor_url(api_vendor, format: :json)
end
