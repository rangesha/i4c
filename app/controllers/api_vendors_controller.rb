class ApiVendorsController < ApplicationController
  before_action :set_api_vendor, only: [:show, :edit, :update, :destroy]

  # GET /api_vendors
  # GET /api_vendors.json
  def index
    @api_vendors = ApiVendor.all
  end

  # GET /api_vendors/1
  # GET /api_vendors/1.json
  def show
  end

  def stocktwits_auth
    vendor = ApiVendor.find_by name: "stocktwits"
    stocktwits = StockTwit.new(vendor.client_id, vendor.client_secret)
    @auth_url = stocktwits.auth_redirect_url
=begin
    respond_to do |format|
      format.html { redirect_to @auth_url }
    end
=end
  end

  def stocktwits_code
    @code = params[:code]
    #TODO: Cleanup with before filter
    vendor = ApiVendor.find_by name: "stocktwits"
    stocktwits = StockTwit.new(vendor.client_id, vendor.client_secret)
    @access_token = stocktwits.auth_receive_code @code
  end

  # GET /api_vendors/new
  def new
    @api_vendor = ApiVendor.new
  end

  # GET /api_vendors/1/edit
  def edit
  end

  # POST /api_vendors
  # POST /api_vendors.json
  def create
    @api_vendor = ApiVendor.new(api_vendor_params)

    respond_to do |format|
      if @api_vendor.save
        format.html { redirect_to @api_vendor, notice: 'Api vendor was successfully created.' }
        format.json { render :show, status: :created, location: @api_vendor }
      else
        format.html { render :new }
        format.json { render json: @api_vendor.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /api_vendors/1
  # PATCH/PUT /api_vendors/1.json
  def update
    respond_to do |format|
      if @api_vendor.update(api_vendor_params)
        format.html { redirect_to @api_vendor, notice: 'Api vendor was successfully updated.' }
        format.json { render :show, status: :ok, location: @api_vendor }
      else
        format.html { render :edit }
        format.json { render json: @api_vendor.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /api_vendors/1
  # DELETE /api_vendors/1.json
  def destroy
    @api_vendor.destroy
    respond_to do |format|
      format.html { redirect_to api_vendors_url, notice: 'Api vendor was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_api_vendor
      @api_vendor = ApiVendor.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def api_vendor_params
      params.require(:api_vendor).permit(:name, :client_id, :client_secret)
    end
end
