class StockTwit < ActiveRecord::Base
  require 'oauth2'
  include HTTParty
  base_uri 'https://api.stocktwits.com'
  default_options.update(verify: false)


  def initialize(client_id, client_secret)
    @client_id = client_id
    @client_secret = client_secret
    @base_uri = 'https://api.stocktwits.com'
  end

  #get the code
  def auth_redirect_url
    #self.class.get "/api/2/oauth/authorize", query: @auth_options
    client = OAuth2::Client.new(
      @client_id,
      @client_secret,
      :site => @base_uri,
      :authorize_url => "/api/2/oauth/authorize",
      :ssl => {:verify => false}
    )
    client.auth_code.authorize_url(:redirect_uri => 'http://localhost:3000/stocktwits/code')
  end

  def auth_receive_code code
    self.class.post("/api/2/oauth/token?client_id=#{@client_id}&client_secret=#{@client_secret}&code=#{code}&grant_type=authorization_code&redirect_uri=http://localhost:3000/stocktwits/success")
  end

end
