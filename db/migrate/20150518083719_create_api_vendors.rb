class CreateApiVendors < ActiveRecord::Migration
  def change
    create_table :api_vendors do |t|
      t.string :client_id
      t.string :client_secret

      t.timestamps null: false
    end
  end
end
