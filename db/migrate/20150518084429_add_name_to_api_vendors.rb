class AddNameToApiVendors < ActiveRecord::Migration
  def change
    add_column :api_vendors, :name, :string
  end
end
